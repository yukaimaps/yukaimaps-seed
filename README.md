Yukaimaps development seed
==========================

Development seed for all the [Yukaimaps](https://gitlab.com/yukaimaps/) tools
with Docker compose.


Clone all the things!
---------------------

Rest of the docs assume we are working in dedicated directory, for example:

```sh
mkdir ~/yukaimaps
cd ~/yukaimaps
```

```sh
git clone git@gitlab.com:yukaimaps/yukaimaps-seed.git
git clone git@gitlab.com:yukaimaps/yukaimaps-website.git
git clone git@gitlab.com:yukaimaps/yukaidi.git
git clone git@gitlab.com:yukaimaps/yukaidi-tagging-schema.git
git clone git@gitlab.com:yukaimaps/yukaimaps-translate.git
git clone git@gitlab.com:yukaimaps/yukaimaps-backend.git
git clone git@gitlab.com:yukaimaps/yukaimaps-home.git
```

Rest of the doc assume we are now working in the `yukaimaps-seed` directory

```sh
cd yukaimaps-seed
```


Prepare all the things!
-----------------------

Configure the rails application.

```sh
cp ../yukaimaps-website/config/example.storage.yml ../yukaimaps-website/config/storage.yml
cp ../yukaimaps-website/config/docker.database.yml ../yukaimaps-website/config/database.yml
touch ../yukaimaps-website/config/settings.local.yml
```

Build the docker images

```
docker-compose build
```

Run migrations

```sh
docker-compose run --rm web bundle exec rake db:migrate
```

Run all the things!
-------------------

```
docker-compose up
```

A Yukaimaps user is created with the following credentials:

- email: `user@example.com`
- password: `password`

Exposed urls:

- Keycloak auth server: <http://127.0.0.1:3030> (`admin` / `password`)
- Yukaimaps home: <http://127.0.0.1:1234>
- Yukaimaps backend: <http://127.0.0.1:8000> (Swagger docs:
  <http://127.0.0.1:8000/docs>)
- Yukaidi editor: <http://127.0.0.1:8080>
- Yukaimaps website: <http://127.0.0.1:3000>
- Yukaidi schema (presets and translations): <http://127.0.0.1:9090>
- MailHog catcher web interface (to see sended emails): <http://127.0.0.1:8025>

Warning: it is important to visit Yukaidi editor and home at `127.0.0.1` and
not `localhost` because the first is configured as a Redirect url in the
dedicated OAuth2 Application.


Manage OIDC users
-----------------

Login with the admin credentials into the auth server 
<http://localhost:3030/admin/master/console/#/yukaimaps/users>

- username: `admin`
- password: `password`

**Warning** when creating a user password, deactivate the "Temporary" switch 
for the user to be able to login without resetting its password.

